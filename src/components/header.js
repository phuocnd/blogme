import React from "react";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { useRecoilState } from "recoil";
import { sidenav } from "../recoil/atom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";
import SideNavLink from "./sidenavlink";

export default function Header() {
  const [sideNav, setSideNav] = useRecoilState(sidenav);

  return (
    <div
      className="shadow px-2 md:px-3 py-4 lg:px-5 "
      style={{ backgroundColor: "#20232a" }}
    >
      <div className="flex">
        <div className="w-1/4 text-white">Image</div>
        <div className="w-1/4 text-white">
          <div className="flex flex-wrap">
            <div className="w-1/4 text-white">
              <SideNavLink
                link="/"
                className="truncate text-xl hover:font-bold hover:text-blue-400 "
                style={{ fontSize: "16px" }}
                activeClassName="rounded border-b-4 border-blue-500 pl-2 font-bold"
              >
                Home
              </SideNavLink>
            </div>
            <div className="w-1/4 text-white">
              <SideNavLink
                link="/Blog"
                className="truncate text-xl hover:font-bold hover:text-blue-400 "
                style={{ fontSize: "16px" }}
                activeClassName="rounded border-b-4 border-blue-500 pl-2 font-bold"
              >
                Blog
              </SideNavLink>
            </div>
          </div>
        </div>
      </div>
      {/* <Link to="/" className="flex justify-center p-4">
        <img src="" alt="Zalo" style={{ width: "60px", height: "31px" }} />
      </Link>
      <div className="px-4">
        <hr className="mb-4" />
      </div> */}
      {/* <span
        className="text-xl block cursor-pointer"
        style={{ marginLeft: "300px" }}
        onClick={() => setSideNav(!sideNav)}
      >
        <FontAwesomeIcon icon={faBars} className="text-white" />
      </span> */}
    </div>
  );
}
