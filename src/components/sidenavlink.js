import React, { useMemo, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useRouter } from "../hooks/useRouter";

export default function SideNavLink({
  className,
  activeClassName,
  link,
  children,
}) {
  const { history, location } = useRouter();
  const [pathName, setPathName] = useState();
  useEffect(() => {
    setPathName(location.pathname);
  }, [location.pathname]);

  useEffect(() => {
    return history.listen((location) => {
      setPathName(location.pathname);
    });
  }, [history]);

  const _className = useMemo(() => {
    if (location.pathname === link) {
      return `${className} ${activeClassName}`;
    }
    return `${className}`;
  }, [className, activeClassName, pathName]);
  return (
    <div className={_className}>
      <Link to={link}>{children}</Link>
    </div>
  );
}
