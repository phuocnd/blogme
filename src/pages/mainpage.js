import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import { useRecoilValue } from "recoil";
import { sidenav } from "../recoil/atom";
import PageStep from "../components/pagestep";
export default function MainPage({ match, location }) {
  const sideNav = useRecoilValue(sidenav);
  useEffect(() => {
    console.log(location);
  }, []);
  const html =
    '<p style="text-align:justify;"><span style="color: rgba(0,0,0,0.84);background-color: rgb(255,255,255);font-size: 20px;font-family: Roboto;"><strong>Các yếu tố ảnh hưởng đến Web Loading?</strong></span></p><p style="text-align:justify;"><span style="color: rgba(0,0,0,0.84);background-color: rgb(255,255,255);font-size: 18px;font-family: Roboto;">Trong thực tế, một website được đánh giá là nhanh hay chậm phụ thuộc vào rất nhiều yếu tố khách quan hoặc chủ quan như:</span></p><ul><li style="text-align:justify;" dir = "auto"><span style="color: rgba(0,0,0,0.84);background-color: rgb(255,255,255);font-size: 18px;font-family: Roboto;">Network</span></li><li style="text-align:justify;" dir = "auto"><span style="color: rgba(0,0,0,0.84);background-color: rgb(255,255,255);font-size: 18px;font-family: Roboto;">Thermal Throttling</span></li><li style="text-align:justify;" dir = "auto"><span style="color: rgba(0,0,0,0.84);background-color: rgb(255,255,255);font-size: 18px;font-family: Roboto;">Parsing JavaScript</span></li><li style="text-align:justify;" dir = "auto"><span style="color: rgba(0,0,0,0.84);background-color: rgb(255,255,255);font-size: 18px;font-family: Roboto;">3rd-party code</span></li><li style="text-align:justify;" dir = "auto"><span style="color: rgba(0,0,0,0.84);background-color: rgb(255,255,255);font-size: 18px;font-family: Roboto;">Device hardware</span></li><li style="text-align:justify;" dir = "auto"><span style="color: rgba(0,0,0,0.84);background-color: rgb(255,255,255);font-size: 18px;font-family: Roboto;">Caching</span></li><li style="text-align:justify;" dir = "auto"><span style="color: rgba(0,0,0,0.84);background-color: rgb(255,255,255);font-size: 18px;font-family: Roboto;">Images</span></li><li style="text-align:justify;" dir = "auto"><span style="color: rgba(0,0,0,0.84);background-color: rgb(255,255,255);font-size: 18px;font-family: Roboto;">…</span></li></ul>';

  return (
    <div className="w-full">
      <div className="flex flex-wrap justify-center">
        <div
          className="w-1/2 p-4"
          style={{ marginLeft: sideNav ? "" : "600px" }}
        >
          <PageStep />
          <div className="flex item-center justify-between mt-6 text-4xl font-bold">
            Những phương pháp tối ưu hóa hiệu năng của website
          </div>
          <div className="mt-2 text-xs">
            {<strong>{new Date().toDateString()}</strong>} by{" "}
            {<strong>Phuocnd</strong>}
          </div>
          <div className="my-4">
            <hr />
          </div>
          <div
            className="mt-2"
            dangerouslySetInnerHTML={{
              __html: html,
            }}
          ></div>
        </div>
      </div>
    </div>
  );
}
