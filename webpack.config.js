import webpack from "webpack";
import HtmlWebpackPlugin from "html-webpack-plugin";
import path from "path";
import config from "./config/development";

export default {
  mode: "development",
  resolve: {
    extensions: ["*", ".js", ".jsx", ".json"],
  },
  entry: [
    // must be first entry to properly set public path
    "webpack-hot-middleware/client?reload=true",
    path.resolve(__dirname, "src/index.js"), // Defining path seems necessary for this to work consistently on Windows machines.
  ],
  target: "web", // necessary per https://webpack.github.io/docs/testing.html#compile-and-test
  output: {
    path: path.resolve(__dirname, "dist"), // Note: Physical files are only output by the production build task `npm run build`.
    publicPath: "/",
    filename: "bundle.js",
  },
  plugins: [
    new webpack.DefinePlugin(config),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new HtmlWebpackPlugin({
      // Create HTML file that includes references to bundled CSS and JS.
      template: "public/index.ejs",
      minify: {
        removeComments: true,
        collapseWhitespace: true,
      },
      inject: true,
    }),
  ],
  module: {
    rules: [
      { test: /\.jsx?$/, exclude: /node_modules/, loaders: ["babel-loader"] },
      { test: /\.eot(\?v=\d+.\d+.\d+)?$/, loader: "file-loader" },
      { test: /(\.css)$/, loaders: ["style-loader", "css-loader?sourceMap"] },
    ],
  },
};
