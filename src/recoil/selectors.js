import { selector } from "recoil";
import { sidenav } from "./atom";

export const sideNavSelector = selector({
  key: "sideNavSelector",
  get: ({ get }) => {
    const state = get(sidenav);
    return state;
  },
});
