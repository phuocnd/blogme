import { atom } from "recoil";

export const sidenav = atom({
  key: "sidenav",
  default: true,
});
