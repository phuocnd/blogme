import React from "react";
import { useRecoilState, useRecoilValue } from "recoil";
import { sidenav } from "../recoil/atom";
import { sideNavSelector } from "../recoil/selectors";

export default function Test() {
  const [testValue, setTestValue] = useRecoilState(sidenav);
  const value = useRecoilValue(sideNavSelector);

  return <div>{testValue}</div>;
}
