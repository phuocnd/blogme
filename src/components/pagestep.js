import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight } from "@fortawesome/free-solid-svg-icons";

export default function PageStep() {
  return (
    <div className="text-base text-gray-500">
      <Link to="/">Home</Link> <FontAwesomeIcon icon={faChevronRight} />{" "}
      <Link to="/">Zalo</Link> <FontAwesomeIcon icon={faChevronRight} />{" "}
      <Link to="/">Nhung yeu to bla bla</Link>
    </div>
  );
}
