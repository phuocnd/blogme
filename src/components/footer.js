import React from "react";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { useRecoilState } from "recoil";
import { sidenav } from "../recoil/atom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";

export default function Footer() {
  const [sideNav, setSideNav] = useRecoilState(sidenav);

  return (
    <div
      className="flex shadow px-2 md:px-3 py-4 lg:px-5 items-center justify-between"
      style={{ backgroundColor: "#20232a" }}
    >
      {/* <Link to="/" className="flex justify-center p-4">
        <img src="" alt="Zalo" style={{ width: "60px", height: "31px" }} />
      </Link>
      <div className="px-4">
        <hr className="mb-4" />
      </div> */}
      {/* <span
        className="text-xl block cursor-pointer"
        style={{ marginLeft: "300px" }}
        onClick={() => setSideNav(!sideNav)}
      >
        <FontAwesomeIcon icon={faBars} className="text-white" />
      </span> */}
    </div>
  );
}
