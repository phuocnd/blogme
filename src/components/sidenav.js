import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import SideNavLink from "./sidenavlink";
import { Link } from "react-router-dom";
import { useRecoilState, useSetRecoilState } from "recoil";
import { sidenav } from "../recoil/atom";
import { systemTabs } from "../pages/tabs";
import { Route, Switch, Redirect } from "react-router-dom";

export default function SideNav() {
  const basePath = "/system";

  return (
    <div
      className="fixed h-full border text-black text-base sidenav"
      style={{ fontSize: "16px" }}
    >
      <div className="flex flex-wrap overflow-auto mt-6 text-xl">
        {systemTabs.map((tab, idx) => (
          <>
            {tab.divided && <hr className="my-2" />}
            <Redirect from="/" exact to="/system/contracttype/1" />
            <SideNavLink
              key={idx}
              link={basePath + tab.path + "/1" + (tab.query || "")}
              className="truncate whitespace-no-wrap lg:w-full px-4 py-2 inline-block md:block text-sm hover:font-bold hover:bg-gray-200 "
              style={{ fontSize: "16px" }}
              activeClassName="rounded border-r-4 border-blue-500 pl-2 font-bold"
            >
              {tab.name}
            </SideNavLink>
          </>
        ))}
      </div>
    </div>
  );
}
