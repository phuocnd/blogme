import React from "react";

export default function RelateContent() {
  return (
    <div
      className="float-right p-2 h-full"
      style={{ backgroundColor: "#f7f7f7" }}
    >
      <div className="mt-64">
        <div className="text-xs text-base font-bold">Related Content</div>
        <div className="bg-blue-400 my-4" style={{ height: "2px" }}></div>

        <div className="flex flex-wrap ">
          <div className="w-full my-2 p-2 border-l-2 border-blue-500">
            Content 1
          </div>
          <div className="w-full my-2 p-2 border-l-2 border-blue-500">
            Content 2
          </div>
          <div className="w-full my-2 p-2 border-l-2 border-blue-500">
            Content 3
          </div>
        </div>
      </div>
    </div>
  );
}
