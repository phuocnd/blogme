import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import "./style/tailwind.css";
import "./style/style.css";

import { i18n } from "element-react";
import locale from "element-react/src/locale/lang/en";
import { RecoilRoot } from "recoil";
import { IntlProvider } from "react-intl";

i18n.use(locale);
ReactDOM.render(
  <Router>
    <IntlProvider locale="vi">
      <RecoilRoot>
        <App />
      </RecoilRoot>
    </IntlProvider>
  </Router>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
