import React from "react";
import ReactDOM from "react-dom";
import { Switch, Route } from "react-router-dom";
import SideNav from "./components/sidenav";
import MainPage from "./pages/mainpage";
import Test from "./components/testRecoil";
import Header from "./components/header";
import Footer from "./components/footer";
import { useRecoilValue } from "recoil";
import { sidenav } from "./recoil/atom";
import { systemTabs } from "./pages/tabs";
import RelateContent from "./components/relatecontent";

export default function App() {
  const sideNav = useRecoilValue(sidenav);
  console.log(sideNav);
  const basePath = "/system";
  return (
    <div className="main h-screen">
      <Header />
      <div
        className="app h-full"
        style={{
          transform: sideNav ? "translateX(0px)" : "translateX(-300px)",
        }}
      >
        <SideNav />
        <RelateContent />
        <div className="w-full h-full">
          <Switch>
            {systemTabs.map((tab, idx) => (
              <Route
                key={idx}
                path={basePath + tab.path + "/:id"}
                component={tab.component}
              />
            ))}
          </Switch>
        </div>
      </div>
      <Footer />
    </div>
  );
}
